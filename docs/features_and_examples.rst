features and use-cases
**********************

the portions of this namespace are simplifying your Python application or service in the areas/domains::

    * data processing/validation
    * file handling
    * i18n (localization)
    * configuration settings
    * console
    * logging
    * database access
    * networking
    * multi-platform/-OS
    * context help
    * app tours
    * user preferences (font size, color, theming, ...)
    * QR codes
    * sideloading


screenshots of apps developed with ae portions
**********************************************


.. list-table::

    * - .. figure:: img/glsl_tester_demo.gif
           :scale: 90 %
           :target: img/glsl_tester_demo.gif

           GlslTest app demo


.. list-table::

    * - .. figure:: img/kivy_lisz_root.png
           :alt: root list of a dark themed kivy lisz app
           :scale: 30 %
           :target: img/kivy_lisz_root.png

           kivy lisz app root list

      - .. figure:: img/kivy_lisz_fruits.png
           :alt: fruits sub-list of a dark themed kivy lisz app
           :scale: 30 %
           :target: img/kivy_lisz_fruits.png

           fruits sub-list

      - .. figure:: img/kivy_lisz_fruits_light.png
           :alt: fruits sub-list of a light themed kivy lisz app
           :scale: 30 %
           :target: img/kivy_lisz_fruits_light.png

           using light theme

    * - .. figure:: img/kivy_lisz_user_prefs.png
           :alt: user preferences drop down
           :scale: 30 %
           :target: img/kivy_lisz_user_prefs.png

           lisz user preferences

      - .. figure:: img/kivy_lisz_color_editor.png
           :alt: kivy lisz color editor
           :scale: 30 %
           :target: img/kivy_lisz_color_editor.png

           kivy lisz color editor

      - .. figure:: img/kivy_lisz_font_size_big.png
           :alt: lisz app using bigger font size
           :scale: 30 %
           :target: img/kivy_lisz_font_size_big.png

           bigger font size


.. list-table::
    :widths: 27 66

    * - .. figure:: img/enaml_lisz_fruits_sub_list.png
           :alt: fruits sub-list of dark themed enaml lisz app
           :scale: 27 %
           :target: img/enaml_lisz_fruits_sub_list.png

           enaml/qt lisz app

      - .. figure:: img/enaml_lisz_light_landscape.png
           :alt: fruits sub-list of a light themed enaml lisz app in landscape
           :scale: 66 %
           :target: img/enaml_lisz_light_landscape.png

           light themed in landscape
